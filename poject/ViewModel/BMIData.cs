﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace poject.ViewModel
{
    public class BMIData
    {
        [Display(Name ="體重")]
        [Required(ErrorMessage = "請輸入登入帳號")]
        [StringLength(12, ErrorMessage = "請勿超過12個字")]

        public float Weight { get; set; }
        public float Height { get; set; }
        public float BMI { get; set; }
        public string Level { get; set; }
    }

}